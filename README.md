# ABM for epidemics

This project develops and Agent-based simulator of epidemics dynamics in a human population. Here, a set of agents (humans) interact with each other in a georeferenced environment were contacts between agents can potentially spread a contagious disease. The characteristics of this geographic environment, the population dynamics and the within-host disease evolution are described here. The program can tract the number of infected agents at each time step, and also its geographical position.

The following figures show output of two different models. They both have the same parameters, except that in the right model, the agents do not go out to recreation places and stay at their homes instead. Details of the model are explained in the following paragraphs.

![Agents neighborhood](/source/images/runs_comparison.png)

*Figure 1 - Two runs of the model: the difference is that in the right model, agents do not go to recreation places*

*Disclaimer: None of this outputs or model represents any realistic situation whatsoever*

## Agents & environment

An agent that has characteristics that define its behavior and movements over time. In this model, it is susceptible of being infected by a disease, and spread it to other agents as well. The characteristics of the spreading and the disease evaolution within an agent can be also dependent on this agent's characteristics: typically the age, but also other parameters such as, prior medical conditions, weight, gender, etc.

The environment is formed by a geographical setting of locations, where agents can spend their time and interact with agents in that same location and in neighboring locations as well. Each agent has a signed a fixed home location, that can share with other agents (i.e. family), a fixed work/school location depending on its age and a random recreation location that can vary for each day (see Figure 1). At each location the agent interacts with other agents during the time spent there, originating potentially infectious contact with a dynamic explain later here.

![Agents neighborhood](/source/images/city_layout_2.png)

*Figure 1 - Agents environment*

## Between-host propagation

At a given time step in the simulation, each agent will have a specific spatial location and will only interact, in terms of potentially infectious contacts with other agents in its 'proximity'. The extent of this neighborhood depends on the environment where the agents are placed at that time step:

1. Open environment: In an open environment all agents are part of the same continuous space. The neighborhood for a given agent is formed by all agents that are closer in distance than a certain threshold value $`i_r`$ that defines the spatial extension of the interactions.
2. Closed environment: The neighborhood of the agent is formed by all agents inside the same closed environment. Inside this cosed environments, exact spatial position of the agents is irrelevant. All agents have the same neighbors in this context.

![Agents neighborhood](/source/images/agents_neighborhood.png)

*Figure 2 - Agent's neighborhoods*

Within a given neighborhood, contacts between pairs of agents (i.e. sufficiently close contacts enough for disease transmission) are assumed to occur following a homogeneous Poisson process. For a close environment this rate is considered proportional to the number of neighbors and equal to $`a_c=\beta_c/N`$ where $`N`$ is the total number of agents in the neighborhood; while for open environments, the rate is considered independent of the population, and equal to $`a_o=\beta_o`$ (These are typical assumptions in population dynamics. See [[1]](#1)) and [[2]](#2)). Thus, a given susceptible agent have contacts with another specific agent of the neighborhood at a rate $`a/N`$. If out of the $`N`$ neghbors, there are $`I`$ infected agents, then the rate of contacts specifically with infected agents is $`a I / N`$. We can then calculate the probability of getting infected as the probability of having, at least, one contact with an infected agent in a given timespan (i.e. time step). That is,

- For closed environments:  $`p^* = 1 - e^{-\beta I \Delta t}`$

- For open environments:    $`p^* = 1 - e^{-\beta (I/N) \Delta t}`$

### Infectivity

In the equations above, we have assumed that one contact with an infected agent is enough to transmit the disease between agents. However, we want to model that different infected agents might have different capacity of transmitting the disease to a susceptible agent: a symptomatic infected might transmit easier than an asymptomatic one; or an agent wearing a facemask would transmit less than one not wearing it.

We define this capacity as the 'infectivity' $`i`$ of the agent. If a single contact is enough to transmit the disease, then that infected agent has $`i=1`$, and would yield the same result as explained before. An agent that cannot transmit the disease (susceptible, recovered, latent) can be thought as an agent with $`i=0`$. In the middle between $`i=0`$ and $`i=1`$ different cases can be considered.

Then, each contact with an infected agent will result in a transmission with probability $`i`$. This is similar to assume that infectious contact with a specific agent $`k`$ occurs at a reduced rate $`a \cdot i_k`$. Again, assuming independence in the contacts between agents, the resulting rate for infectious contacts is $`a \sum_{k} i_k`$, where $` \sum_{k} i_k `$ is the sum of the infectivities of all its neighbors. The probability of infection transmission for a susceptible agent can be rewritten as,

- For closed environments:  $`p^* = 1 - e^{-\beta \sum_{k} i_k \Delta t } `$

- For open environments:    $`p^* = 1 - e^{-\beta \left( \sum_{k} i_k / N \right) \Delta t } `$

Naturally, the agents with $`i_k=0`$ (cannot transmit) will not add to the probability of infection. In the case that the agents are either non-transmisors ($`i=0`$) or perfect transmisors ($`i=1`$), the equation yields the same as the first result: $` \sum_{k} i_k = I`$.

## Within-host evolution

The most used mathematical model to describe the disease behavior in the agents is through compartmental models (See [[1]](#1)). Each agent is classified based on a discrete set of disease stages, or health status. In our model, this compartments are described in Figure 2 and can be described as:

- Susceptible (S): An agent that has not been exposed to the infection, and does not have developed a resistance towards the disease, is labeled as susceptible. 
- Exposed (E): Once it is infected, it enters an asymptomatic stage that has virtually no infectious capacity, and is labeled as exposed; this stage lasts for a period of $`l_E`$ units of time with probability distribution $`f_{l_E}`$.
- Infectious: After the exposed period, an agent enters an infectious stage where it can transmit the disease to susceptible agents; this stage lasts for a period of $`l_I`$ units of time, with probability distribution $`f_{l_I}`$.
    - Asymptomatic (A):  An agent does not present any symptons with probability $`p_A`$, and it is thus labeled 'Asymptomatic'.
    - Symptomatic Mild (S1): An agent can present mild symptoms with probability $`p_{S1}`$. This stage might trigger certain actions in the agents like skipping work or moving less around its environment.
    - Symptomatic Severe (S2): An agent can present severe symptoms with probability $`p_{S2}=1-p_A-p_{S2}`$. This stage might trigger certain actions in the agents like complete isolation in household and hospital internation.
- Removed: After the infectious period, the disease dissapears from the agent, resulting in two possible outcomes.
    - Recovered (R): An agent might have recovered from the disease and develop a resistance towards it. It is no longer susceptible of catching it again, and thus it is labelled as 'Recovered'. We assumed that all A and S1 agents recover with probability $`1`$, and S2 with probability $`p_{R|S_2}`$
    - Dead (D): An agent that does not recovers from the disease might end up dead, and labelled as such. It cannot transmit or receive the disease anymore. We assume that only agents that present sever symptoms might end up dead with probability $`p_D=1-p_{R|S_2}`$

![Agents neighborhood](/source/images/disease_evolution_net.png)

*Figure 3 - Health status*

This model can be added further complexity suchs as extra levels of symptoms, or infectious length parameters dependent on the infectious stage, at the expense of adding more parameters to be calibrated.

## Society



## Bibliography
<a id="1">[1]</a> 
Ouboter, T.; Meester, R & Trapman, P. (2016)
Stochastic SIR epidemics in a population with households and schools.
Journal of Mathematical Biology, (72) 1177-1193.

<a id="2">[2]</a> 
Castillo-Chavez, C. (2012).
Mathematical Approaches for Emerging and Reemerging Infectious Diseases.
Springer Science & Bussiness Media
